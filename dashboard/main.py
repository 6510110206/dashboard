import pandas as pd
import plotly.graph_objs as go
import dash
import dash_html_components as html
import dash_core_components as dcc


data = pd.read_excel('data/data_dash.xlsx')


column_names = ['อ.1', 'อ.2', 'ป.1', 'ป.2', 'ป.3', 'ป.4', 'ป.5', 'ป.6', 'ม.1', 'ม.2', 'ม.3', 'ม.4', 'ม.5', 'ม.6']


def create_pie_chart(column):
    labels = data['ประเภทความพิการ']
    values = data[column]
    colors = ['#FFD700', '#FFA500', '#FF8C00', '#FF6347', '#FA8072', '#DC143C', '#9400D3', '#4B0082', '#00BFFF']
    return go.Figure(data=[go.Pie(labels=labels[0:-2], values=values, textinfo='label+percent', 
                                  marker=dict(colors=colors))])


app = dash.Dash(__name__)


app.layout = html.Div([
    html.H2('รายงานตามประเภทความพิการ  สถานศึกษา :  โสตศึกษาจังหวัดสุรินทร์'),
    dcc.Graph(id='pie-chart'),
    dcc.Slider(
        id='column-slider',
        min=0,
        max=len(column_names)-1,
        step=1,
        value=0,
        marks={i: column_names[i] for i in range(len(column_names))}
    )
])


@app.callback(
    dash.dependencies.Output('pie-chart', 'figure'),
    [dash.dependencies.Input('column-slider', 'value')])
def update_pie_chart(column_index):
    column = column_names[column_index]
    fig = create_pie_chart(column)
    return fig

if __name__ == '__main__':
    app.run_server(debug=True)
